<h1>Identifikasi dan ranking use case dari proses bisnis aplikasi bukukas


<table>
  <tr>
    <th> Use Case </th>
    <th> Nilai Prioritas </th>
  </tr>
  <tr>
    <td>User dapat memasukan data stok barang</td>
    <td>10</td>
  </tr>
  <tr>
    <td>User dapat menginput aliran dana masuk</td>
    <td>8</td>
  </tr>
    <tr>
    <td>User dapat menginput aliran dana keluar</td>
    <td>8</td>
  </tr>
    <tr>
    <td>User dapat melihat laporan uang keluar</td>
    <td>9</td>
  </tr>
    <tr>
    <td>User dapat melihat laporan uang masuk</td>
    <td>9</td>
  </tr>
</table>
