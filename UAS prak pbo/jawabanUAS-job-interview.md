[](url)<h1>NO 1
<h2>Identifikasi dan ranking use case dari proses bisnis aplikasi tokopedia seller</h2>


<h3>use case user</h3>
<table>
  <tr>
    <th> Use Case user </th>
    <th> Nilai Prioritas </th>
  </tr>
  <tr>
    <td>Mendaftar sebagai penjual</td>
    <td>10</td>
  </tr>
  <tr>
    <td>Mengelola produk</td>
    <td>8</td>
  </tr>
    <tr>
    <td>Mengelola pesanan</td>
    <td>9</td>
  </tr>
    <tr>
    <td>Membuat promosi dan diskon</td>
    <td>8</td>
  </tr>
    <tr>
    <td>Memonitor performa penjualan</td>
    <td>6</td>
  </tr>
    <tr>
    <td>Menganalisis data penjualan</td>
    <td>4</td>
    
  </tr>
</table>

<h3>use case manajemen</h3>
<table>
  <tr>
    <th> Use Case manajemen </th>
    <th> Nilai Prioritas </th>
  </tr>
  <tr>
    <td>Manajemen keuangan dan akuntansi</td>
    <td>10</td>
  </tr>
  <tr>
    <td>Manajemen sumber daya manusia (SDM)</td>
    <td>9</td>
  </tr>
    <tr>
    <td>Manajemen proyek</td>
    <td>9</td>
  </tr>
    <tr>
    <td>Manajemen rantai pasokan</td>
    <td>8</td>
  </tr>
    <tr>
    <td>Manajemen pemasaran</td>
    <td>7</td>
  </tr>
    <tr>
    <td>Manajemen operasional</td>
    <td>6</td>
    
  </tr>
</table>

<h3>use case direksi perusahaan (dashboard, monitoring, analisis)</h3>
<table>
  <tr>
    <th> Use Case direksi perusahaan (dashboard, monitoring, analisis) </th>
    <th> Nilai Prioritas </th>
  </tr>
  <tr>
    <td>Dashboard Eksekutif</td>
    <td>10</td>
  </tr>
  <tr>
    <td>Monitoring Kinerja Perusahaan</td>
    <td>9</td>
  </tr>
    <tr>
    <td>Analisis Keuangan</td>
    <td>9</td>
  </tr>
    <tr>
    <td>Analisis Data Pelanggan</td>
    <td>7</td>
  </tr>
    <tr>
    <td>Pemantauan Rantai Pasokan</td>
    <td>7</td>
  </tr>
</table>

<h1>NO 2
<h2>Identifikasi Class diagram dari proses bisnis aplikasi tokopedia seller</h2>
<img src=https://gitlab.com/madman-mitch/pbo/-/raw/main/UAS%20prak%20pbo/CD.png><br>

<h1>NO 3
<h2>Identifikasi SOLID Design Principle</h2>
<img src=https://gitlab.com/madman-mitch/pbo/-/raw/main/UAS%20prak%20pbo/sdp.png><br>

<h1>NO 4
<h2>Identifikasi Design Pattern yang dipilih</h2>
<img src=https://gitlab.com/madman-mitch/pbo/-/raw/main/UAS%20prak%20pbo/dP.png><br>
MVC FLOW:<br>
→ Dalam file Admincontroller.java & userController.java terdapat fungsi berbasis pemetaan yang mengembalikan nama file.<br>
→ Dalam direktori Src->main->webapp->views, file JSP berikut akan dieksekusi.

<h1>NO 5
<h2>Identifikasi konektivitas ke database</h2>
<img src=https://gitlab.com/madman-mitch/pbo/-/raw/main/UAS%20prak%20pbo/kon_DB.png><br>

<h1>NO 6
<h2>Identifikasi pembuatan web service dan setiap operasi CRUD</h2>
<h3>Pembuatan web service<br>
<img src=https://gitlab.com/madman-mitch/pbo/-/raw/main/UAS%20prak%20pbo/WS_spring.png><br>
pembuatan CRUD<br>
<img src=https://gitlab.com/madman-mitch/pbo/-/raw/main/UAS%20prak%20pbo/crud.png><br>
<h1>NO 7
<h2>Identifikasi Graphical User Interfacer</h2>
<img src=https://gitlab.com/madman-mitch/pbo/-/raw/main/UAS%20prak%20pbo/js.png><br>
GUI yang dihasilkan adalah antarmuka pengguna berbasis web yang dibangun dengan menggunakan HTML, CSS, dan JavaScript, contoh pengunaan seperti pada gambar diatas. JSP berfungsi sebagai template yang digunakan untuk menggabungkan logika bisnis Java dengan markup HTML yang akan dikirimkan ke browser sebagai tampilan akhir.


<h1>NO 8
<h2>Identifikasi HTTP connection melalui GUI</h2>


<h1>NO 9
<h2>demonstrasi Melalui Youtube</h2>
[](https://youtu.be/0jXf8UdriZE)

<h1>NO 10
<h2>BONUS</h2>
[](https://colab.research.google.com/drive/1uUEvrzec6yYLMxEC7XmwIcEkSsp1hkNg#scrollTo=gaMwbw7zuKbs)
